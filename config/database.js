// config/database.js
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('pm_admin', 'postgres', 'bibo3kali', {
  host: 'localhost',
  dialect: 'postgres',
});

module.exports = sequelize;
