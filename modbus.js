const modbus = require('jsmodbus');
const net = require('net');
var Cacheman = require('cacheman');
let path = require('path')
var cache = new Cacheman();
var bodyParser = require('body-parser')
const express = require('express');
const cors = require('cors');
const app = express();
const corsOptions = {
    // default 10.10.100.254
    origin: ['http://192.168.1.70:9000', 'http://192.168.1.70:3000', '*'], // List allowed origins
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE', // Allowed HTTP methods
    allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization', // Allowed headers
};
app.use(express.static(path.join(__dirname, 'public')));
// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(cors(corsOptions));
const socket = new net.Socket();
const options = {
    'host': '192.168.1.70',
    'port': '502'
};

const client = new modbus.client.TCP(socket);
const http = require('http'); // Require the 'http' module
const server = http.createServer(app); // Create an HTTP server
const io = require('socket.io')(server, {
    cors: {
        origin: '*',
    }
}); // Integrate Socket.io with the server
const sequelize = require('./config/database');

async function getQuery() {
    const rawQuery = `
          SELECT m.id AS machine_id, m.name AS machine, m.set_id AS set_id,
          d.id AS panel_id, d.name AS panel_name, d.address AS panel_address
          FROM "machines" m
          LEFT JOIN "paneldata" d ON m.id = d."machine_id"
          ORDER BY m.id, d.id`;

    const result = await sequelize.query(rawQuery, {
        logging: false,
        type: sequelize.QueryTypes.SELECT,
    });
    cache.set('resQuery', result);
    //redis.set("resQuery", "test");
}

getQuery();

async function cardContainers() {
    const customArray = [];

    let currentMachine = null;
    const resQuery = await cache.get("resQuery");
    //cache.get("resQuery").then((result) => {
    resQuery.forEach(row => {
        if (currentMachine === null || currentMachine.machine_id !== row.machine_id) {
            if (currentMachine !== null) {
                currentMachine.panel_data = panelData;
                customArray.push(currentMachine);
            }

            panelData = [];
            currentMachine = {
                machine_id: row.machine_id,
                machine: row.machine,
                set_id: row.set_id,
                panel_data: [],
            };
        }

        if (row.panel_id !== null && row.panel_address !== null) {
            panelData.push({
                id: row.panel_id,
                name: row.panel_name,
                address: row.panel_address,
            });
        }
    });
    // Handle the last machine
    if (currentMachine !== null) {
        currentMachine.panel_data = panelData;
        customArray.push(currentMachine);
    }
    return customArray;
    //io.emit('cardContainer', JSON.stringify(customArray));

}


//setTimeout(function () {
async function streamData() {

    const customArray = [];

    let currentMachine = null;
    let panelData = [];
    const resQuery = await cache.get("resQuery");
    //cache.get("resQuery").then((result) => {
    resQuery.forEach(row => {
        if (currentMachine === null || currentMachine.machine_id !== row.machine_id) {
            if (currentMachine !== null) {
                currentMachine.panel_data = panelData;
                customArray.push(currentMachine);
            }

            panelData = [];
            currentMachine = {
                machine_id: row.machine_id,
                machine: row.machine,
                set_id: row.set_id,
                panel_data: [],
            };
        }

        if (row.panel_id !== null && row.panel_address !== null) {
            panelData.push({
                id: row.panel_id,
                name: row.panel_name,
                address: row.panel_address,
            });
        }
    });
    // });

    // Handle the last machine
    if (currentMachine !== null) {
        currentMachine.panel_data = panelData;
        customArray.push(currentMachine);
    }

    // Process the Modbus data for each panel address
    for (const machine of customArray) {
        for (const panel of machine.panel_data) {
            try {
                if (panel.address !== "null" && panel.address !== "") {
                    const resp = await client.readHoldingRegisters(panel.address, 2);
                    const data = resp.response._body.valuesAsArray;
                    const buffer = new ArrayBuffer(4);
                    const view = new DataView(buffer);
                    view.setInt16(1, data[0])
                    view.setInt16(0, data[1]) // notice: setInt16
                    val = view.getFloat32(0).toFixed(2);
                    panel.value = val; // Add Modbus data to panel object
                }
            } catch (err) {
                //console.error("Error:", err);
                // Handle the error as needed
            }
        }
    }

    // Now, customArray contains the desired structure with Modbus data
    ////console.log(JSON.stringify(customArray, null, 2));

    // Emit the structured data to clients via Socket.io
    io.emit('FromAPI', JSON.stringify(customArray));
    //io.emit('FromAPI', "etes");
    cache.set('resQuery', resQuery);
}

setInterval(() => {
    saveInterval()
}, 1000);

async function saveInterval() {
    const customArray = [];

    let currentMachine = null;
    let panelData = [];
    const resQuery = await cache.get("resQuery");
    //cache.get("resQuery").then((result) => {
    resQuery.forEach(row => {
        if (currentMachine === null || currentMachine.machine_id !== row.machine_id) {
            if (currentMachine !== null) {
                currentMachine.panel_data = panelData;
                customArray.push(currentMachine);
            }

            panelData = [];
            currentMachine = {
                machine_id: row.machine_id,
                machine: row.machine,
                set_id: row.set_id,
                panel_data: [],
            };
        }

        if (row.panel_id !== null && row.panel_address !== null) {
            panelData.push({
                machine_id: currentMachine.machine_id,
                id: row.panel_id,
                name: row.panel_name,
                address: row.panel_address,
            });
        }
    });
    // });

    // Handle the last machine
    if (currentMachine !== null) {
        currentMachine.panel_data = panelData;
        customArray.push(currentMachine);
    }

    // Process the Modbus data for each panel address
    for (const machine of customArray) {
        for (const panel of machine.panel_data) {
            try {
                if (panel.address !== "null" && panel.address !== "") {
                    const resp = await client.readHoldingRegisters(panel.address, 2);
                    const data = resp.response._body.valuesAsArray;
                    const buffer = new ArrayBuffer(4);
                    const view = new DataView(buffer);
                    view.setInt16(1, data[0])
                    view.setInt16(0, data[1]) // notice: setInt16
                    val = view.getFloat32(0).toFixed(2);
                    panel.value = val; // Add Modbus data to panel object
                }
            } catch (err) {
                //console.error("Error:", err);
                // Handle the error as needed
            }
        }
    }

    const rawQuery = `select * from reset_time`;

    const result = await sequelize.query(rawQuery, {
        logging: false,
        type: sequelize.QueryTypes.SELECT,
    });
    const time_use = JSON.parse(result[0].time_use);
    const array = ["10:05", "11:06", "13:08"];

    const now = new Date();
    const currentHour = now.getHours();
    const currentMinutes = now.getMinutes();

    const currentTime = `${currentHour < 10 ? '0' : ''}${currentHour}:${currentMinutes < 10 ? '0' : ''}${currentMinutes}`;
    //console.log(customArray.length)

    if (time_use.includes(currentTime)) {
        /* customArray.forEach(getMachines => {
            // console.log(machines.panel_data)
            console.log(getMachines)
            const mc = getMachines;
           
        });*/
        for (i = 0; i <= customArray.length - 1; i++) {
            //if (i <= customArray.length) {
            //console.log(`${i} - ${customArray[i].panel_data.length}`);
            for (iData = 0; iData <= customArray[i].panel_data.length - 1; iData++) {
                //console.log(`${customArray[i].panel_data[iData].name}`);
                toInsert(customArray[i].panel_data[iData])
            }
            //}
        }

        //console.log(`The array contains the current time (${currentTime}).`);
    }
    //console.log(JSON.parse(result[0].time_use)[2])

}

async function toInsert(panelData) {
    const now = new Date();
    const time = `${now.getDate()}-${now.getMonth()}-${now.getFullYear()} ${now.getHours()}:${now.getMinutes()}`;
    const rawQuery = `select * from report_energy where name = '${panelData.name}' and created_at = '${time}'`;

    const result = await sequelize.query(rawQuery, {
        logging: false,
        type: sequelize.QueryTypes.SELECT,
    });
    if (result.length == 0) {
        console.log("kosong")
        await sequelize.query(`INSERT INTO report_energy (machine_id, created_at, name, val) values ('${panelData.machine_id}', '${time}', '${panelData.name}', '${panelData.value}')`, {
            raw: true,
            type: 'INSERT'
        })
    } else {
        console.log("isi")
    }
    console.log(result);
    /* await sequelize.query(`INSERT INTO report_energy (machine_id, created_at, name, val) values ('${panelData.machine_id}', '${time}', '${panelData.name}', '${panelData.value}')`, {
        raw: true,
        type: 'INSERT'
    }) */
}

// pages
app.get("/home", function (req, res) {
    //res.json(cardContainers);
    res.sendFile(path.join(__dirname, "public/index.html"));
});
app.get("/setting", function (req, res) {
    //res.json(cardContainers);
    res.sendFile(path.join(__dirname, "public/setting.html"));
});
app.get("/report", function (req, res) {
    //res.json(cardContainers);
    res.sendFile(path.join(__dirname, "public/report.html"));
});


// reporting data
app.get("/getData", async function (req, res) {
    try {
        const data = await cardContainers(); // Await the result of cardContainers function
        res.json(data); // Send the JSON response with the retrieved data
    } catch (error) {
        res.status(500).send({ error: "Internal Server Error" }); // Handle error if any
    }
});

app.post("/saveTime", urlencodedParser, async function (req, res) {

    try {

        await sequelize.query(`truncate table reset_time`);
        await sequelize.query(`INSERT INTO reset_time (type, time_use, changed) values ('-','${req.body.time}', now())`, {
            raw: true,
            type: 'INSERT'
        });
        var resultData = {
            status: "ok",
            message: "Berhasil disimpan",
        };

        res.json(resultData);
    } catch (error) {
        res.json(error)
    }

});

app.post("/saveDay", urlencodedParser, async function (req, res) {
    try {

        await sequelize.query(`UPDATE reset_time SET type = 'day', time_use = '${req.body}'  WHERE id = 1`, {
            raw: true,
            type: 'UPDATE',
        });
        var resultData = {
            status: "ok",
            message: "Berhasil disimpan",
        };
        console.log(resultData)
        res.json(resultData)
    } catch (error) {
        res.json(error)
    }
});

app.post("/saveWeek", urlencodedParser, function (req, res) {
    try {

        var resultData = {
            status: "ok",
            message: "Berhasil disimpan",
            data: req.body
        };
        console.log(resultData)
        res.json(resultData)
    } catch (error) {
        res.json(error)
    }
});

app.post("/saveMonth", urlencodedParser, function (req, res) {
    try {

        var resultData = {
            status: "ok",
            message: "Berhasil disimpan",
            data: req.body
        };
        console.log(resultData)
        res.json(resultData)
    } catch (error) {
        res.json(error)
    }
});

app.get("/getResetTime", async function (req, res) {
    const rawQuery = `select * from reset_time`;

    const result = await sequelize.query(rawQuery, {
        logging: false,
        type: sequelize.QueryTypes.SELECT,
    });
    res.json(result);
});

socket.on('error', console.error);
socket.connect(options, () => {
    ////console.log("Connected to Modbus server");
    setInterval(function () {
        streamData(); // Start streaming data
    }, 4000);

});

app.get("/getReportEnergy", async function (req, res) {
    const rawQuery = `SELECT machine_id, created_at,machines.name,
    MAX(CASE WHEN report_energy.name = 'V1' THEN report_energy.val END) AS V1,
    MAX(CASE WHEN report_energy.name = 'V2' THEN report_energy.val END) AS V2,
    MAX(CASE WHEN report_energy.name = 'V3' THEN report_energy.val END) AS V3,
    MAX(CASE WHEN report_energy.name = 'A1' THEN report_energy.val END) AS A1,
    MAX(CASE WHEN report_energy.name = 'A2' THEN report_energy.val END) AS A2,
    MAX(CASE WHEN report_energy.name = 'A3' THEN report_energy.val END) AS A3,
    MAX(CASE WHEN report_energy.name = 'KWH' THEN report_energy.val END) AS KWH
FROM report_energy left join machines on report_energy.machine_id=machines.id
GROUP BY report_energy.machine_id, report_energy.created_at, machines.id
ORDER BY report_energy.machine_id, report_energy.created_at`;

    const result = await sequelize.query(rawQuery, {
        logging: false,
        type: sequelize.QueryTypes.SELECT,
    });
    res.json(result);
});

// Set up Socket.io event listeners
io.on('connection', (socket) => {
    ////console.log('Client connected');

    // Handle disconnection (optional)
    socket.on('disconnect', () => {
        ////console.log('Client disconnected');
    });
});

// Listen on a port (e.g., 3000)
server.listen(10000, () => {
    ////console.log('Server is running on port 10000');
});
